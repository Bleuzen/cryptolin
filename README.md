# With this application you can encrypt texts or files and chat with friends. #

## **Downloads** ##
Go to the [Downloads](https://bitbucket.org/Bleuzen/cryptolin/downloads) page and
click on Cryptolin#.exe ("#" is the version).

"#" is the version.


## **How to start** ##

*You need Java 8!
If you haven´t already, you can download it from here:*

*http://java.com/download/*

(# = versionnumber)

### Windows :###
double-click on Cryptolin#.exe

### Linux Mint / Ubuntu (Terminal) :###
*java -jar Cryptolin#.exe*

### Other :###
Not testet, but you can try

*java -jar Cryptolin#.exe*

### Console :###
*java -jar Cryptolin#.exe --help*

example: 
for setting up a chat server (for example on a raspberry pi with java) type:

*java -jar Cryptolin#.exe --start-chat-server*

if you want to use another port type

*java -jar Cryptolin#.exe --start-chat-server <YOUR_PORT>*

to encrypt a text:

*java -jar Cryptolin#.exe --encrypt-text <key> <text>*
hint: use "%n" for a new line

to decrypt a text

*java -jar Cryptolin#.exe --decrypt-text <key> <text>*

to check for updates

*java -jar Cryptolin#.exe --version*

...


## **Contact** by email:
*supgesu@gmail.com*